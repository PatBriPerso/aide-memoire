const am_config = {
  todoistApiKey: '${TODOIST_API_KEY}',
  fetchTasksIfNeverUpdateSeconds: '${FETCH_TASKS_IF_NEVER_UPDATE_SECONDS}',
  fetchTasksIfNoTaskSeconds: '${FETCH_TASKS_IF_NO_TASK_SECONDS}',
  fetchTasksIntervalSeconds: '${FETCH_TASKS_INTERVAL_SECONDS}',
  backToClockDelaySeconds: '${BACK_TO_CLOCK_DELAY_SECONDS}',
  showNextTaskBeforeSeconds: '${SHOW_NEXT_TASK_BEFORE_SECONDS}',
  showNextTaskEverySeconds: '${SHOW_NEXT_TASK_EVERY_SECONDS}',
}
