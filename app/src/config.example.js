const am_config = {
  todoistApiKey: 'my_key',
  fetchTasksIfNeverUpdateSeconds: '30',
  fetchTasksIfNoTaskSeconds: '60',
  fetchTasksIntervalSeconds: '120',
  backToClockDelaySeconds: '10',
  showNextTaskBeforeSeconds: '1800',
  showNextTaskEverySeconds: '30',
  useDebugTasks: false,
  debugTasks: [
    {
      "id": 224,
      "content": "[DEBUG] Atelier mémoire",
      "description": "Pour maintenir les fonctions cognitives.\nDans la salle du 2ème étage.",
      "completed": false,
      "priority": 1,
      "created": "2022-04-20T14:07:56Z",
      "due": {
        "date": "2022-04-29",
        "datetime": "2022-04-29T09:00:00Z",
      }
    },
    {
      "id": 868,
      "content": "[DEBUG] Atelier gymnastique douce",
      "description": "Un atelier pour réveiller votre corps en douceur. Dans le salon du RdC.",
      "completed": false,
      "priority": 1,
      "created": "2022-04-20T14:11:24Z",
      "due": {
        "date": "2022-04-26",
        "datetime": "2022-04-26T11:30:00Z",
      }
    },
    {
      "id": 240,
      "content": "[DEBUG] Atelier équilibre",
      "description": "Un atelier dédié à la marche et à la prévention du risque de chute. Dans le salon du RdC.",
      "completed": false,
      "priority": 1,
      "created": "2022-04-20T14:17:13Z",
      "due": {
        "date": "2022-04-27",
        "datetime": "2022-04-27T12:30:00Z",
      }
    }
  ]  
}
