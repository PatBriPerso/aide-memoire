# aide-memoire

This Balena app allow to display a clock and a tasks list.
The clock shows the current weekday, the time and the current date. Clicking on the time toggle the seconds display.
The tasks list is download from Todoist so you need to configure your Todoist API Key on the Balena device configuration.

## TODOs

- Add text to speech. See: 
  - https://codecoda.com/en/blog/entry/text-to-speech-with-javascript
  - https://responsivevoice.org/



## Project setup
```
lando start
```

Website is accessible at http://aide-memoire.lndo.site/

## Develop locally with balena

See: https://www.balena.io/docs/learn/develop/local-mode/

1. Use the development variant of the balena OS
2. Enable Local mode for the device from balena.io
3. Find the device with `sudo balena scan`

You should see this output:
```
Scanning for local balenaOS devices... Reporting scan results
- 
  host:          b07fdd7.local
  address:       192.168.23.154
  osVariant:     development
  dockerInfo: 
    Containers:        5
    ContainersRunning: 5
    ContainersPaused:  0
    ContainersStopped: 0
    Images:            17
    Driver:            overlay2
    SystemTime:        2022-03-06T17:35:30.555289938Z
    KernelVersion:     5.10.78-v8
    OperatingSystem:   balenaOS 2.88.5+rev1
    Architecture:      aarch64
  dockerVersion: 
    Version:    19.03.30
    ApiVersion: 1.40
```
The device is `b07fdd7.local`.

4. Push the version to this device with `balena push b07fdd7.local`

